package com.pavliuk.epam.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomSymbolService {

  private Random random;
  private List<String> symbols;

  public RandomSymbolService() {
    this.random = new Random();
    this.symbols = new ArrayList<String>();
    symbols.add("@");
    symbols.add("$");
    symbols.add("^");
    symbols.add("x");
    symbols.add("0");
    symbols.add("%");
    symbols.add("&");
    symbols.add("#");
  }

  public String getSymbol() {
    return symbols.get(random.nextInt(symbols.size()));
  }

}
