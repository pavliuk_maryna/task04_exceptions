package com.pavliuk.epam.view.menu;

import com.pavliuk.epam.controller.GameService;
import com.pavliuk.epam.controller.RandomSymbolService;
import com.pavliuk.epam.exception.WrongTurnInputException;
import com.pavliuk.epam.model.Player;
import java.util.Scanner;

public class GameMenu {

  private GameService gameService;
  private RandomSymbolService randomSymbolService;
  private Scanner scanner;
  private Player player2;
  private Player player1;

  public GameMenu() {
    this.gameService = new GameService();
    this.scanner = new Scanner(System.in);
    this.randomSymbolService = new RandomSymbolService();
    this.player1 = new Player();
    this.player2 = new Player();
  }

  public void run() {
    while (true) {
      System.out.println(
          "Hello! This is Tic Tac Toe, the best game in the world! \nDo you want to play? Y/N");
      if (scanner.next().equals("Y")) {
        while (true) {
          System.out.println("Enter player 1 name");
          player1.setName(scanner.next());
          player1.setSymbol(randomSymbolService.getSymbol());
          printPlayerInfo(player1);
          System.out.println("Enter player 2 name");
          player2.setName(scanner.next());
          while (true) {
            String symbol = randomSymbolService.getSymbol();
            if (!player1.getSymbol().equals(symbol)) {
              player2.setSymbol(symbol);
              break;
            }
          }
          printPlayerInfo(player2);
          System.out.println("Lets start the game");
          boolean finished = false;
          while (!finished) {
            System.out.println(player1.getName()
                + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
            gameService.printGameField();
            makeTurn(player1.getSymbol());
            finished = isFinished(gameService);
            if (finished) {
              break;
            }
            System.out.println(player2.getName()
                + " it's your turn, please write: horizontal+vertical choice, example: 0+2");
            gameService.printGameField();
            makeTurn(player2.getSymbol());
            finished = isFinished(gameService);
          }
          System.out.println("Restart? Y/N");
          if (scanner.next().equals("Y")) {
            gameService.fillGameField();
          } else {
            break;
          }
        }
      } else {
        System.out.println("Bye! Have a beautiful day!");
        break;
      }
    }
  }

  private boolean isFinished(GameService gameService) {
    if (gameService.win(player1.getSymbol())) {
      gameService.printGameField();
      System.out.println(player1.getName() + " you won!");
      return true;
    } else if (gameService.win(player2.getSymbol())) {
      gameService.printGameField();
      System.out.println(player2.getName() + " you won!");
      return true;
    } else if (gameService.isDraw(player1.getSymbol(), player2.getSymbol())) {
      gameService.printGameField();
      System.out.println("Draw!");
      return true;
    }
    return false;
  }

  private void makeTurn(String symbol) {
    while (true) {
      try {
        String choice = scanner.next();
        checkTurnInput(choice);
        String[] split = choice.split("\\+");
        int horizontalCoordinate = Integer.parseInt(split[0]);
        int verticalCoordinate = Integer.parseInt(split[1]);
        if (!gameService.isCoordinateEmpty(horizontalCoordinate, verticalCoordinate)) {
          System.out.println("There is already a symbol present in this cell, please try again!");
        } else {
          gameService.putSymbol(symbol, horizontalCoordinate, verticalCoordinate);
          break;
        }
      } catch (WrongTurnInputException e) {
        System.out.println("Your specified input is wrong, try again, example: 0+2");
      }
    }
  }

  private void checkTurnInput(String turn) {
    if (!turn.matches("[0-2]\\+[0-2]")) {
      throw new WrongTurnInputException("Input does not match [0-2]+\\+[0-2] regex");
    }
  }

  private void printPlayerInfo(Player player) {
    System.out.println("Player name: " + player.getName() + " and symbol: " + player.getSymbol());
  }

}
