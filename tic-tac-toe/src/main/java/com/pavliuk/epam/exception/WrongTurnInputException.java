package com.pavliuk.epam.exception;

public class WrongTurnInputException extends RuntimeException {

  public WrongTurnInputException(String message) {
    super(message);
  }
}
